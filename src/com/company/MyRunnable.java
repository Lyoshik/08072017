package com.company;

public class MyRunnable implements Runnable{

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Tester.test();
            System.out.println("Hello! My name is " + Thread.currentThread().getName()
                        + " You calling a method " + Tester.a + " times!");
        }
    }
}
